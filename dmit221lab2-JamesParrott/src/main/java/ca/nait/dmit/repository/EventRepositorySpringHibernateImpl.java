package ca.nait.dmit.repository;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;




import ca.nait.dmit.domain.Event;


import util.HibernateUtil;

@Repository
public class EventRepositorySpringHibernateImpl implements EventRepository {
	
	private SessionFactory sessionFactory;
	
	@Autowired
	public EventRepositorySpringHibernateImpl(SessionFactory sessionFactory)
	{
		this.sessionFactory = sessionFactory;
	}
	
	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	@Transactional(readOnly=true)
	public long count() throws Exception {
		// TODO Auto-generated method stub
		return (long) getCurrentSession().createQuery("select count(eventId) from Event").uniqueResult();
	}

	@Override
	@Transactional
	public void delete(Event event) throws Exception {
		// TODO Auto-generated method stub
		getCurrentSession().delete(event);
	}

	@Override
	@Transactional
	public void delete(String eventId) throws Exception {
		// TODO Auto-generated method stub
		Event event = findOne(eventId);
		delete(event);
	}

	@Override
	@Transactional
	public void deleteAll() throws Exception {
		// TODO Auto-generated method stub
		getCurrentSession().createQuery("DELETE FROM Event").executeUpdate();
	}

	@Override
	@Transactional(readOnly=true)
	public boolean exists(String studentId) throws Exception {
		// TODO Auto-generated method stub
		return getCurrentSession().createQuery(
				"from Event where eventId = :evevntIdValue")
				.setString("eventIdValue", studentId)
				.uniqueResult() != null;
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Event> findAll() throws Exception {
		// TODO Auto-generated method stub
		return getCurrentSession().createQuery("from Event ORDER BY eventDateTime ").list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Event> findAllByName(String partialEventName)
			throws Exception {
		// TODO Auto-generated method stub
		return getCurrentSession().createQuery("from Event where title like :partialValue")
				.setString("partialValue", "%" + partialEventName + "%")
				.list();
	}

	@Override
	@Transactional(readOnly=true)
	public Event findOne(String eventId) throws Exception {
		// TODO Auto-generated method stub
		return (Event) getCurrentSession().get(Event.class, eventId);
	}

	@Override
	@Transactional
	public void save(Event event) throws Exception {
		// TODO Auto-generated method stub
		getCurrentSession().saveOrUpdate(event);
	}

}
