package ca.nait.dmit.repository;

import java.util.List;

import ca.nait.dmit.domain.Event;

/**
 * Interface for CRUD operations on a repository for the Course type.
 * 
 * @author swu
 */
public interface EventRepository {

	/**
	 * Return the number of courses available.
	 * @return the number of courses available
	 */
	long count() throws Exception;
	
	/**
	 * Delete a given course
	 * @param course
	 */
	void delete(Event event) throws Exception;

	/**
	 * Delete the course with the given courseId
	 * @param courseId must not be null
	 */
	void delete(String eventId) throws Exception;
	
	/**
	 * Delete all courses managed by the repository.
	 */
	void deleteAll() throws Exception;
	
	/**
	 * Returns whether an course with the given courseId exists.
	 * @param courseId must not be null
	 * @return if an course with given id exists, false otherwise
	 */
	boolean exists(String eventId) throws Exception;
	
	/**
	 * Return all courses
	 * @return all courses
	 */
	List<Event> findAll() throws Exception;

	/**
	 * Return all courses where the name contains the partialCourseName 
	 * @param partialCourseName
	 * @return
	 */
	List<Event> findAllByName(String partialEventName) throws Exception;

	/**
	 * Retrieves an course by its courseId
	 * @param course
	 * @return the course with the given courseId or null if none found
	 */
	Event findOne(String eventId) throws Exception;

	/**
	 * Saves a given course.
	 * @param course
	 */
	void save(Event event)  throws Exception;

}
