package ca.nait.dmit.domain;
 
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
 
import javax.persistence.*; // for JPA annotations
 
@Entity
@Table(name="exercises_useraccount")
public class UserAccount implements Serializable {
    private static final long serialVersionUID = 1L;
     
    @Id
    @Column(unique=true, nullable=false, length=100)
    private String username;
     
    @Column(nullable=false)
    private String password;
     
    @ElementCollection(fetch=FetchType.EAGER)
    @CollectionTable(name="exercises_userrole", joinColumns=@JoinColumn(name="username"))
    @Column(name="rolename",length=25,nullable=false)
    private Set<String> userRoles = new HashSet<>();
     
    public String getUsername() {
        return username;
    }
 
    public void setUsername(String username) {
        this.username = username;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password) {
        this.password = password;
    }
 
    public Set<String> getUserRoles() {
        return userRoles;
    }
 
    public void setUserRoles(Set<String> userRoles) {
        this.userRoles = userRoles;
    }
 
    public UserAccount() {
        super();
    }
     
}
