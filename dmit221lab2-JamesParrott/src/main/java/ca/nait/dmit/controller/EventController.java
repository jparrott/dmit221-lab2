package ca.nait.dmit.controller;



import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.beans.factory.annotation.Autowired;

import util.JSFUtil;

import ca.nait.dmit.domain.Event;
import ca.nait.dmit.repository.EventRepository;
import ca.nait.dmit.repository.EventRepositorySpringHibernateImpl;


@org.springframework.stereotype.Component("eventBean")
@org.springframework.context.annotation.Scope("session")
public class EventController {

	@Autowired
	private EventRepository eventRepository; //EventRepositoryJdbcImpl.getInstance();
	private Event currentEvent = new Event();
	
	private List<Event> events = new java.util.ArrayList<Event>();
	
	private Date date1;
	
	
	
	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public Event getCurrentEvent() {
		return currentEvent;
	}
	
	public void setCurrentEvent(Event currentEvent) {
		this.currentEvent = currentEvent;
	}
	
	public List<Event> getEvents() {
		
		return events;
		} 
	

	public EventController() {
		super();
		try {
			events = eventRepository.findAll();
			} catch (Exception e) {
			JSFUtil.addErrorMessage("The following exception has occurred: " + e.getMessage());
			}
	}

	public void setEvents(List<Event> events) {
		this.events = events;
	}

	public void saveEvent() {
		try {
			eventRepository.save(currentEvent);
			events = eventRepository.findAll();
			currentEvent = new Event();
			JSFUtil.addInfoMessage("Successfully saved event information.");
		} catch (Exception e) {
			JSFUtil.addErrorMessage("Error! The following exception has occurred: " + e.getMessage() );
		}
	}
	
	public void deleteEvent() {
		try {
		eventRepository.delete(currentEvent);
		events = eventRepository.findAll();
		currentEvent = new Event();
		JSFUtil.addInfoMessage("Successfully deleted record.");
		} catch (Exception e) {
		JSFUtil.addErrorMessage("Error! The following exception has occurred: " + e.getMessage() );
		}
		}

		public String createNew() {
		currentEvent = new Event();
		return "/saveEvent?faces-redirect=true";
		}
}
