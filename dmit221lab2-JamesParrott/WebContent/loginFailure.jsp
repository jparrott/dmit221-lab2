<%
	Integer loginCount = 1;
	if( session.getAttribute("loginCount") != null )
	{
		loginCount = (Integer) session.getAttribute("loginCount");
	}
	loginCount++;
	session.setAttribute("loginCount", loginCount );
	if( loginCount > 3 )
	{
		request.getRequestDispatcher("/bannedUser.jsp").forward(request, response);
	}
	else
	{
		request.setAttribute("failure", "Invalid username and/or password. Log in attempt #" + loginCount );
		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}
%>
