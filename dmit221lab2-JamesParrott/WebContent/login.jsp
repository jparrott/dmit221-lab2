<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Login</title>
</head>
<body>
	${failure}
				
	<h4>Please enter your username and password.</h4>	
	<form action="j_security_check" method="post" >
		<table>
		    <tr><td>UserName:</td>
		        <td><input type="text" name="j_username" /></td>
		    </tr>
		    <tr><td>Password:</td>
		        <td><input type="password" name="j_password" /></td>
		    </tr>
		    <tr><td colspan="2" align="right"><input type="submit" value="Login" /></td>
		    </tr>
		</table>
	</form>
	<script type="text/javascript">
		document.forms[0].j_username.focus();
	</script>	

</body>
</html>